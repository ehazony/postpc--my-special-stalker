package com.example.myspecialstalker;

import android.Manifest;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Build;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.widget.EditText;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class MainActivity extends AppCompatActivity {
    private static final int REQUEST_CODE_PERMISSION = 1546;
    public static final String CHANNEL_ID = "14445";
    public MutableLiveData<Boolean> messageLiveData;

    private static final String TEXT = "TEXT";
    private static final String PHONE_NUMBER = "PHONE_NUMBER";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        final MainActivity activity = this;
        boolean hasSmsPermissionSMS =
                ActivityCompat.checkSelfPermission(activity, Manifest.permission.SEND_SMS) ==
                        PackageManager.PERMISSION_GRANTED;
        boolean hasSmsPermissionPhoneState =
                ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_PHONE_STATE) ==
                        PackageManager.PERMISSION_GRANTED;
        boolean hasSmsPermissionOutgoingCalls =
                ActivityCompat.checkSelfPermission(activity, Manifest.permission.PROCESS_OUTGOING_CALLS) ==
                        PackageManager.PERMISSION_GRANTED;




        if (hasSmsPermissionSMS && hasSmsPermissionOutgoingCalls && hasSmsPermissionPhoneState) {
            setContentView(R.layout.activity_main);

        } else {
            if (!hasSmsPermissionSMS) {
                ActivityCompat.requestPermissions(
                        activity,
                        new String[]{Manifest.permission.SEND_SMS},
                        REQUEST_CODE_PERMISSION);
            }
            if (!hasSmsPermissionOutgoingCalls) {
                ActivityCompat.requestPermissions(
                        activity,
                        new String[]{Manifest.permission.PROCESS_OUTGOING_CALLS},
                        REQUEST_CODE_PERMISSION);
            }
            if (!hasSmsPermissionPhoneState) {
                ActivityCompat.requestPermissions(
                        activity,
                        new String[]{Manifest.permission.READ_PHONE_STATE},
                        REQUEST_CODE_PERMISSION);
            }
        }
        EditText textMessage = (EditText) findViewById(R.id.editTextMessage);
        final EditText textNumber = (EditText) findViewById(R.id.editTextphoneNumber);
        final TextView textViewNumber = (TextView) findViewById(R.id.numberRedyText);



        textMessage.setText(sp.getString(TEXT, "I'm going to call this number:"));
        String number = sp.getString(PHONE_NUMBER, "");
        if(!number.equals("")){
            String tex ="the app is ready to send SMS messages to this number: "+number;
            textViewNumber.setText(tex);
        }
        textNumber.setText(number);

        textMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                SharedPreferences.Editor editor = sp.edit();
                editor.putString(TEXT, s.toString());
                editor.apply();
            }
        });
        textNumber.addTextChangedListener(new PhoneNumberFormattingTextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (10 == s.length() || (s.length() ==13&&s.charAt(0)=='+' && s.charAt(1) == '9' && s.charAt(2)== '7' && s.charAt(3) == '2')) {
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString(PHONE_NUMBER, s.toString());
                    editor.apply();
                    String tex ="the app is ready to send SMS messages to this number: "+s.toString();
                    textViewNumber.setText(tex);

                }


            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        createNotificationChannel();

    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode,
            @NonNull String[] permissions,
            @NonNull int[] grantResults
    ) {
        // we know we asked for only 1 permission, so we will surely get exactly 1 result
        // (grantResults.size == 1)
        // depending on your use case, if you get only SOME of your permissions
        // (but not all of them), you can act accordingly

        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            setContentView(R.layout.activity_main);
        } else {
            // the user has denied our request! =-O


            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.SEND_SMS)) {
                // reached here? means we asked the user for this permission more than once,
                // and they still refuse. This would be a good time to open up a dialog
                // explaining why we need this permission
            }
        }


    }





    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
}


