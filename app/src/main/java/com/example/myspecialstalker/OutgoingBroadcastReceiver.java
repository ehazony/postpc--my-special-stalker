package com.example.myspecialstalker;

import android.app.Activity;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.telephony.SmsManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

public class OutgoingBroadcastReceiver extends BroadcastReceiver {

    private static final String TEXT = "TEXT";
    private static final String PHONE_NUMBER = "PHONE_NUMBER";
    public static final String CHANNEL_ID = "14445";
    private Context context;
    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        String sendPhoneNumber = sp.getString(PHONE_NUMBER, null);
        String sendText = sp.getString(TEXT, null);
        // notification set up



        if (sendPhoneNumber != null && sendText!= null) {
            if (intent.getAction().equals(Intent.ACTION_NEW_OUTGOING_CALL)) {
                // If it is to call (outgoing)
                String callingPhoneNumber = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
                sendSms( sendPhoneNumber,sendText + callingPhoneNumber);
            }
        }
    }

    public void sendSms(String sendPhoneNumber, String text) {
        final int notificationId = 14445;
        // define get broadcast of send and deiliverd
        PendingIntent sentPI = PendingIntent.getBroadcast(context, 0, new Intent("SMS_SENT"), 0);
        PendingIntent deliveredPI = PendingIntent.getBroadcast(context, 0, new Intent("SMS_DELIVERED"), 0);

        // registerReceiver message sent
        context.registerReceiver(new BroadcastReceiver(){
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode())
                {
                    case Activity.RESULT_OK:
                        Notification ntfc = new NotificationCompat.Builder(context, CHANNEL_ID)
                                .setContentTitle("message sent successfully!").setSmallIcon(R.drawable.ic_launcher_background)
                                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                                .build();
                        NotificationManagerCompat.from(context).notify(notificationId, ntfc);
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(context, "Generic failure",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(context, "No service",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(context, "Null PDU",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(context, "Radio off",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter("SMS_SENT"));

        // registerReceiver message delivered
        context.registerReceiver(new BroadcastReceiver(){
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode())
                {
                    case Activity.RESULT_OK:
                        Notification ntfc = new NotificationCompat.Builder(context, CHANNEL_ID)
                                .setContentTitle("message received successfully!").setSmallIcon(R.drawable.ic_launcher_background)
                                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                                .build();
                        NotificationManagerCompat.from(context).notify(notificationId, ntfc);
                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(context, "SMS not delivered",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter("SMS_DELIVERED"));

        //sent notification
        Notification ntfc = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setContentTitle("sending message..").setSmallIcon(R.drawable.ic_launcher_background)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .build();
        NotificationManagerCompat.from(context).notify(notificationId, ntfc);


        //sending message
        SmsManager.getDefault().sendTextMessage(
                sendPhoneNumber,
                null,
                text,
                sentPI,
                deliveredPI);
    }
}

